const express = require('express'); 
// require('dotenv/config'); 
const port = process.env.PORT || 3000; 
// Local Modules 
const myRoute = require('./Route/myRoute.js'); 
const myModel = require('./model/connection.js'); 
const schema = require('./model/schema.js');

// Server Initialization 
const app = express(); 
const PORT = process.env.PORT; 
  
// Middlewares 
// .use define a middleware
app.use(express.json()); 
  
// Routes will be written here 
app.use('/route', myRoute);  
  
// Server Listen Along with Database  
// connection(in case of data persistence) 
app.listen(PORT, (error) =>{ 
    
    if(!error) 
        // console.log("Server is Successfully Running,and App is listening on port "+ PORT) 
        console.log(`Server is running on port ${port}`);
    else 
        console.log("Error occurred, server can't start", error); 
    } 
);