// // Require Mongoose
// const mongoose = require("mongoose");

// // Define a schema
// const Schema = mongoose.Schema;

// const SomeModelSchema = new Schema({
//   name: String,
//   a_date: Date,
// });
const mongoose = require("mongoose");


// const mongoose = require('mongoose');

// Define a Mongoose schema
const exampleSchema = new mongoose.Schema({
  name: String,
  age: Number,
});

// Create a Mongoose model based on the schema
const Example = mongoose.model('Example', exampleSchema);
console.log("module");

module.exports = Example;
