const mongoose = require('mongoose');

// MongoDB connection string
const mongoURI = 'mongodb://localhost:27017/your-database';

// Connect to MongoDB
mongoose.connect(mongoURI, { useNewUrlParser: true, useUnifiedTopology: true });

// Check connection
const db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB connection error:'));
db.once('open', () => {
  console.log('Connected to MongoDB');
});
console.log("connect");

// const mongoose = require('mongoose');
// // MongoClient.connect("mongodb://localhost:27017/YourDB", { useNewUrlParser: true })
// mongoose.connect('mongodb://localhost/admin', {useNewUrlParser: true});
// console.log("connect");


